#version 330

// TODO: get color value from vertex shader
in vec3 world_position;
in vec3 world_normal;

// Uniforms for light properties
uniform vec3 light_direction;
uniform vec3 light_position;
uniform vec3 eye_position;
uniform int sp;
uniform float ang;

uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;

uniform vec3 object_color;

layout(location = 0) out vec4 out_color;

void main()
{
	vec3 N = normalize(world_normal);
	vec3 L = normalize( light_position - world_position);
	vec3 V = normalize( eye_position - world_position);
	vec3 H = normalize( L + V );
	
	float cut_off = radians(ang);
	float spot_light = dot(-L, light_direction);
	float spot_light_limit = cos(cut_off);
 
	float linear_att = (spot_light - spot_light_limit) / (1 - spot_light_limit);
	float light_att_factor = pow(linear_att, 2);
	// TODO: define ambient light component
		float ambient_light = 0.25;

		// TODO: compute diffuse light component
		// TODO: compute diffuse light component
		float diffuse_light = 0;
		diffuse_light = material_kd * max(dot(N, L), 0);
		// TODO: compute specular light component
		float specular_light = 0;
		if(diffuse_light > 0)
			specular_light = material_ks * 1 * pow(max(dot(N, H), 0), material_shininess);
	
		float d = distance(light_position, world_position);
		// TODO: compute light
		float f_at = 1 / (d * d);
		float light;
		if(sp == 0)
			light = ambient_light + (specular_light + diffuse_light) * f_at;
		else{
			if (spot_light > spot_light_limit){	
				light = ambient_light + (specular_light + diffuse_light) * light_att_factor;

			}else{
				light = ambient_light;
		
			}
		}
	out_color = vec4(light * object_color, 1);
}