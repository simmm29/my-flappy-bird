#include "Laborator7.h"

#include <vector>
#include <string>
#include <iostream>
#include <Core/Engine.h>

using namespace std;

Laborator7::Laborator7()
{
}

Laborator7::~Laborator7()
{
}

void Laborator7::Init()
{
	camera = new Lab::Camera();
	camera->Set(glm::vec3(0, 2, 3.5f), glm::vec3(0, 1, 0), glm::vec3(0, 1, 0));

	{
		Mesh* mesh = new Mesh("box");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("sphere");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "sphere.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("mare");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "mare.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("teapot");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "teapot.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("plane");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "plane50.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Shader *shader = new Shader("ShaderLab7");
		shader->AddShader("Source/Laboratoare/Laborator7/Shaders/VertexShader.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Laboratoare/Laborator7/Shaders/FragmentShader.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;
	}

	{
		lightPosition = glm::vec3(0, 2, 0);
		materialShininess = 30;
		materialKd = 0.5;
		materialKs = 0.5;
		Ang = 30;
		f = 0;
	}

	b_1 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	b_2 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	b_3 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	c_1 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	c_2 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	c_3 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	c_4 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	c_5 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	u = 0.1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.3 - 0.1)));
	u1 = 0.2 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.4 - 0.2)));
	u2 = 0.1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.3 - 0.1)));
	u3 = 0.2 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.4 - 0.2)));
	u4 = 0.1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.3 - 0.1)));
	u5 = 0.2 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.4 - 0.2)));
	u6 = 0.3 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.5 - 0.3)));
	n = 2 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (7 - 2)));
	projectionMatrix = glm::perspective(RADIANS(180), window->props.aspectRatio, 0.01f, 200.0f);
}

void Laborator7::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(1.000, 0.855, 0.725, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);	
}

void Laborator7::Update(float deltaTimeSeconds)
{
	// camera se misca in continuu de la stanga la dreapta
	camera->TranslateRight(deltaTimeSeconds);
	// daca am apasat pe butonul stang al mouse-ului, daca am miscat mouse-ul in sus, coordonata ty creste
	//(avionul urca), rotindu-se in sus
	if (mouse == 1) {
		if (deltay < 0) {
			mouse = 0;
			ty += deltaTimeSeconds * 3;
			lightPosition -= deltaTimeSeconds * 3;
			ang += deltaTimeSeconds;
		}
		else { // altfel, coordonata ty scade(avionul coboara), rotindu-se in jos
			mouse = 0;
			ang -= deltaTimeSeconds;
			ty -= deltaTimeSeconds * 3;
			lightPosition += deltaTimeSeconds * 3;
		}
	}

	//folosesc variabila tx pentru miscarea continua de la stanga la dreapta a: 
	// avionului, marii, vietilor si combustibilului
	tx += (deltaTimeSeconds / 1000);

	//marea
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0, -2.8, 0));
		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(1.5, 1, 1));
		modelMatrix *= Transform::RotateOY(angie);
		RenderSimpleMesh(meshes["mare"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
	}

	//avionul

	//corp
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		if (ty == 0)
			modelMatrix *= Transform::Translate(0, -2, 0);
		else
			modelMatrix *= Transform::Translate(0, 1, 0);
		modelMatrix *= Transform::Translate(tx, ty, 0);
		modelMatrix *= Transform::RotateOZ(ang);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(1.4, 0.6, 0.6));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.3, 0.3, 0.3));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.647, 0.165, 0.165));
	}

	//coada
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		if (ty == 0)
			modelMatrix *= Transform::Translate(0, -2, 0);
		else
			modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.2, 1.2, 0));
		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, ty, 0));
		modelMatrix *= Transform::RotateOZ(ang);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.5, 0.5));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.804, 0.522, 0.247));

	}

	//aripi
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		if (ty == 0)
			modelMatrix *= Transform::Translate(0, -2, 0);
		else
			modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 1, 0.3));
		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, ty, 0));
		modelMatrix *= Transform::RotateOZ(ang);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5, 0.1, 1));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.824, 0.412, 0.118));

	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		if (ty == 0)
			modelMatrix *= Transform::Translate(0, -2, 0);
		else
			modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 1, -0.2));
		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, ty, 0));
		modelMatrix *= Transform::RotateOZ(ang);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5, 0.1, 1));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.824, 0.412, 0.118));

	}

	//legatura cu elicea
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		if (ty == 0)
			modelMatrix *= Transform::Translate(0, -2, 0);
		else
			modelMatrix = glm::translate(modelMatrix, glm::vec3(0.3, 1, 0));
		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, ty, 0));
		modelMatrix *= Transform::RotateOZ(ang);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.1));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(1, 0, 0));

	}

	//elice
	{
		//se roteste continuu in jurul propriei axe
		angularStep += deltaTimeSeconds;
		glm::mat4 modelMatrix = glm::mat4(1);
		if (ty == 0)
			modelMatrix *= Transform::Translate(0, -2, 0);
		else
			modelMatrix = glm::translate(modelMatrix, glm::vec3(0.35, 1, 0));
		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, ty, 0));
		modelMatrix *= Transform::RotateOZ(ang);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 1.2, 0.05));
		modelMatrix *= Transform::RotateOZ(angularStep * 10);
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.855, 0.647, 0.125));

	}
	// Render the camera target. Useful for understanding where is the rotation point in Third-person camera movement
	if (renderCameraTarget)
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, camera->GetTargetPosition());
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1f));
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));

	}

	//nori

	//viteza de rotire a fiecarui dintre cei 3 nori
	angie += deltaTimeSeconds;
	angie_1 += deltaTimeSeconds * 0.8;
	angie_2 += deltaTimeSeconds * 0.6;

	//de fiecare data cand reapar, cuburile din care sunt formati norii au coordonatele
	// de pe OX si OY diferite, aleatoare
	if (angie >= 3.2) {
		angie = -0.3;
		u = tx + 0.1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.3 - 0.1)));
		u1 = tx + 0.2 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.4 - 0.2)));
	}
	if (angie_1 >= 3.2) {
		angie_1 = -0.3;
		u2 = tx + 0.1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.3 - 0.1)));
		u3 = tx + 0.2 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.4 - 0.2)));
	}
	if (angie_2 >= 3.2) {
		angie_2 = -0.3;
		u4 = tx + 0.1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.3 - 0.1)));
		u5 = tx + 0.2 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.4 - 0.2)));
		u6 = tx + 0.3 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.5 - 0.3)));
	}

	//nor 1
	{
		glm::mat4 modelMatrix = glm::mat4(1);

		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix *= Transform::RotateOY(10);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));

		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(u, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		modelMatrix *= Transform::RotateOX(angie);
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(u1, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		modelMatrix *= Transform::RotateOZ(4);
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}
	
	//nor 2

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie_1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		modelMatrix *= Transform::RotateOX(angie_1);
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(u2, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie_1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		modelMatrix *= Transform::RotateOZ(2);
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}
	{

		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(u3, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie_1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		modelMatrix *= Transform::RotateOZ(1.5);
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}

	//nor 3
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie_2);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}
	{
		float u = tx + 0.1;

		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(u4, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie_2);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		modelMatrix *= Transform::RotateOZ(2);
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}
	{
		float u = tx + 0.3;

		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(u5, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie_2);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		modelMatrix *= Transform::RotateOZ(1.5);
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}
	{
		float u = tx + 0.4;

		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(u6, 0, 0));
		modelMatrix *= Transform::RotateOZ(angie_2);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(2, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		modelMatrix *= Transform::RotateOZ(1.5);
		modelMatrix *= Transform::RotateOX(angie_2);
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0.933, 0.510, 0.933));
	}
	
	//obstacole
	// daca avionul nu e invincibil
	if (ty != 0 && col_bonus2 == 0) {
		// daca nu am avut deja coliziune cu obstacolul curent
		if (coliziune == 0) {
			{
				ok1 = 0;
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(o_1, c_1, 0));
				modelMatrix *= Transform::RotateOY(angie); // se rotesc continuu in jurul centrului lor
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
				RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(1.000, 0.000, 0.000));
			}
		}
		else {
			if (ok1 == 0) {
				k++;
				ok1 = 1;
			}
		}
		// obstacolele merg de la dreapta la stanga, atata timp cat nu ai iesit din fereastra
		if (o_1 >= s_1)
			o_1 -= deltaTimeSeconds * 1.2;
		else { // altfel, coordonatele lor cresc putin si ele o iau de la capat
			
			coliziune = 0;
			s_1 = s_1 + 0.2;
			e_1 = e_1 + 0.2;
			o_1 = e_1;
			c_1 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1))); // pozitia pe OY e mereu alta
		}
		if (coliziune_3 == 0) 
			{
				ok3 = 0;
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(o_3, c_3, 0));
				modelMatrix *= Transform::RotateOY(angie);
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
				RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(1.000, 0.000, 0.000));
		}
		else {
			if (ok3 == 0) {
				k++;
				ok3 = 1;
			}
		}

		if (o_3 >= s_3)
			o_3 -= deltaTimeSeconds * 1.2;
		else {
			/*if (coliziune_3 == 1)
				k++;*/
			coliziune_3 = 0;
			s_3 = s_3 + 0.2;
			e_3 = e_3 + 0.2;
			o_3 = e_3;
			c_3 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
		}
		if (coliziune_2 == 0) 
			{
				ok2 = 0;
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(o_2, c_2, 0));
				modelMatrix *= Transform::RotateOY(angie);
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
				RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(1.000, 0.000, 0.000));
			}
		else {
			if (ok2 == 0) {
				k++;
				ok2 = 1;
			}
		}

		if (o_2 >= s_2)
			o_2 -= deltaTimeSeconds * 1.2;
		else {
			
			coliziune_2 = 0;
			s_2 = s_2 + 0.2;
			e_2 = e_2 + 0.2;
			o_2 = e_2;
			c_2 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));

		}

		//vieti
		// daca am avut 3 coliziuni, inseamna ca nu mai am vieti, deci am pierdut jocul
		if (k == 3) {
			// avionul coboara rapid
			ty -= deltaTimeSeconds * 6;
			ang -= deltaTimeSeconds * 1.5;
		}
		if (ty < -4 && k == 3) {
			cout << "ai pierdut" << '\n';
			exit(1);
		}
		//viata 3
		if (k == 0 || k == 1 || k == 2)
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(tx - 2.2, 2.1, 0));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
			RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
		}
		//viata 2
		if (k == 0 || k == 1)
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(tx - 2.05, 2.1, 0));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
			RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
		}
		//viata 1
		if (k == 0)
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(tx - 1.9, 2.1, 0));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
			RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
		}
	}
	else {
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(o_1, c_1, 0));
				modelMatrix *= Transform::RotateOY(angie);
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
				RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(1.000, 0.000, 0.000));
			}
		if (o_1 >= s_1)
			o_1 -= deltaTimeSeconds * 1.2;
		else {
			s_1 = s_1 + 0.2;
			e_1 = e_1 + 0.2;
			o_1 = e_1;
			c_1 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
		}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(o_3, c_3, 0));
				modelMatrix *= Transform::RotateOY(angie);
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
				RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(1.000, 0.000, 0.000));
			}

		if (o_3 >= s_3)
			o_3 -= deltaTimeSeconds * 1.2;
		else {
			s_3 = s_3 + 0.2;
			e_3 = e_3 + 0.2;
			o_3 = e_3;
			c_3 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
		}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(o_2, c_2, 0));
				modelMatrix *= Transform::RotateOY(angie);
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2, 0.2, 0.2));
				RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(1.000, 0.000, 0.000));
			}

		if (o_2 >= s_2)
			o_2 -= deltaTimeSeconds * 1.2;
		else {
			s_2 = s_2 + 0.2;
			e_2 = e_2 + 0.2;
			o_2 = e_2;
			c_2 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));

		}
		//vieti

		// viata 3
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(tx - 2.2, 2.1, 0));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
			RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
		}
		//viata 2
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(tx - 2.05, 2.1, 0));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
			RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
		}
		//viata 1
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(tx - 1.9, 2.1, 0));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
			RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
		}

	}

	//combustibil

	//1

	if (a_3 >= start_3)
		a_3 -= deltaTimeSeconds * 1.4;
	else {
		col = 0;
		// n este nr de teapot-uri din care e alcatuit un combustibil
		n = 2 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (7 - 2)));
		start_3 = start_3 + 0.2;
		end_3 = end_3 + 0.2;
		a_3 = end_3;
		b_3 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	}
	// daca nu am luat deja acel combustibil (daca l-am luat, el nu mai apare in scena)
	if (col == 0) {

		for(int i = 0; i < n; i++)
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			// formula de calculare si pozitiionare a componentelor combustibilului
			if (i != 0) {
				float u = a_3 + (float)i / (float)10;
				float u_1;
				if (n <= 4 && i == n - 1) {
					u_1 = b_3 + (float)i / (float)100;
				}
				else
					 u_1 = b_3 - 2 * ((float)i / (float)100);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(u, u_1, 0));
			}else
				modelMatrix = glm::translate(modelMatrix, glm::vec3(a_3, b_3, 0));
			modelMatrix *= Transform::RotateOY(angie);
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
			RenderSimpleMesh(meshes["teapot"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
		}


	}
	//2
	if (a_2 >= start_2)
		a_2 -= deltaTimeSeconds * 1.4;
	else {
		kol = 0;
		start_2 = start_2 + 0.2;
		end_2 = end_2 + 0.2;
		a_2 = end_2;
		b_2 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	}
	if (kol == 0) {
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(a_2, b_2, 0));
			modelMatrix *= Transform::RotateOY(angie);
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
			RenderSimpleMesh(meshes["teapot"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
		}

		{
			float u = a_2 + 0.1;
			float u_1 = b_2 - 0.05;
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(u, u_1, 0));
			modelMatrix *= Transform::RotateOY(angie);
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
			RenderSimpleMesh(meshes["teapot"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 1));
		}
	}
	{
		//crestere/ descrestere combustibil
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(tx + 2, 2.1, 0));
		modelMatrix *= Transform::RotateOY(0.1);
		// daca tocmai am accelerat(a inceput jocul), se consuma din combustibil
		if (ty != 0 && flag == 0) {
			flag = 1;
			bust = 1.2;
		}
		// daca tocmai am luat un obiect de combustibil, imi creste combustibilul la maxim
		if (flag == 1) {
			if (col == 1) {
				bust = 1.7;

			}
			else // altfel, el e in continua scadere
				bust -= deltaTimeSeconds * 0.05;
			if (bust <= 0) {
				cout << "nu mai ai combustibil -> ai pierdut\n";
				exit(1);
			}
		}
		modelMatrix = glm::scale(modelMatrix, glm::vec3(bust, 0.7, 0.7));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.15, 0.15, 0.15));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(1.000, 0.753, 0.796));
	}

	// "coliziunile" pentru luarea obiectelor de combustibil
	if (((tx - 0.15) <= (a_3 + 0.05) && (tx + 0.15) >= (a_3 - 0.05)) && (ty <= (b_3 - 1) &&
		ty >= (b_3 - 1.4)) && (-0.15 <= 0.05 && 0.15 >= -0.05)) {
		col = 1;
	}
	if (((tx - 0.15) <= (a_2 + 0.05) && (tx + 0.15) >= (a_2 - 0.05)) && (ty <= (b_2 - 1) &&
		ty >= (b_2 - 1.4)) && (-0.15 <= 0.05 && 0.15 >= -0.05)) {
		kol = 1;
	}
	
	//coliziune


	// verific pe toate cele 3 axe daca: partea stanga a avionului e mai mica decat partea dreapta 
	// a obstacolului si daca partea dreapta a avionului e mai mare decat partea stanga a obstacolului
	if (ty != 0 && col_bonus2 == 0) {
		if (((tx - 0.15) <= (o_1 + 0.1) && (tx + 0.15) >= (o_1 - 0.1)) && (ty <= (c_1 - 1) &&
			ty >= (c_1 - 1.4)) && (-0.15 <= 0.1 && 0.15 >= -0.1))
			coliziune = 1;
		if (((tx - 0.15) <= (o_2 + 0.1) && (tx + 0.15) >= (o_2 - 0.1)) && (ty <= (c_2 - 1) &&
			ty >= (c_2 - 1.4)) && (-0.15 <= 0.1 && 0.15 >= -0.1))
			coliziune_2 = 1;
		if (((tx - 0.15) <= (o_3 + 0.1) && (tx + 0.15) >= (o_3 - 0.1)) && (ty <= (c_3 - 1) &&
			ty >= (c_3 - 1.4)) && (-0.15 <= 0.1 && 0.15 >= -0.1))
			coliziune_3 = 1;
	}

	//bonusuri

	//sfera galbena

	// daca am luat sfera
	if (((tx - 0.15) <= (o_4 + 0.05) && (tx + 0.15) >= (o_4 - 0.05)) && (ty <= (c_4 - 1) &&
		ty >= (c_4 - 1.4)) && (-0.15 <= 0.05 && 0.15 >= -0.05)) {
		q = 1;
		if (ok4 == 0) { // in momentul in care s-a intamplat coliziunea, scad nr de coliziuni k cu 
			// obstacolele, astfel, imi va aparea o viata in plus
			if(k != 0)
				k--;
			ok4 = 1;
		}
	}	
	if (q == 0) {
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(o_4, c_4, 0));
		modelMatrix *= Transform::RotateOY(angie);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab7"], modelMatrix, glm::vec3(1, 1, 0));
	}
	if (o_4 >= s_4)
		o_4 -= deltaTimeSeconds * 1.2;
	else {
		q = 0;
		ok4 = 0;
		s_4 = s_4 + 0.2;
		e_4 = e_4 + 0.2;
		o_4 = e_4;
		c_4 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	}

	//cub verde

	// verificare luare cub verde de catre avion
	if (((tx - 0.15) <= (o_5 + 0.05) && (tx + 0.15) >= (o_5 - 0.05)) && (ty <= (c_5 - 1) &&
		ty >= (c_5 - 1.4)) && (-0.15 <= 0.05 && 0.15 >= -0.05)) {
		col_bonus2 = 1;
	}
	//atata timp cat s-a produs o coliziune cu un cub verde, avionul e invincibil
	if (col_bonus2 == 1) {
		counter += 0.2;
		k = 0; // nu poate avea nicio coliziune cu obstacolele
	}

	if (counter == doi) {
		col_bonus2 = 0;
		counter = 0;
	}
	if (col_bonus2 == 0) {
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(o_5, c_5, 0));
		modelMatrix *= Transform::RotateOY(angie);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
		RenderSimpleMesh(meshes["box"], shaders["ShaderLab7"], modelMatrix, glm::vec3(0, 1, 0));
	}
	if (o_5 >= s_5)
		o_5 -= deltaTimeSeconds * 1.2;
	else {
		s_5 = s_5 + 0.2;
		e_5 = e_5 + 0.2;
		o_5 = e_5;
		c_5 = 1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1.8 - 1)));
	}

}
void Laborator7::FrameEnd()
{

}

void Laborator7::RenderSimpleMesh(Mesh *mesh, Shader *shader, const glm::mat4 & modelMatrix, const glm::vec3 &color)
{
	if (!mesh || !shader || !shader->GetProgramID())
		return;

	// render an object using the specified shader and the specified position
	glUseProgram(shader->program);

	// Set shader uniforms for light & material properties
	// TODO: Set light position uniform
	int light = glGetUniformLocation(shader->program, "light_position");
	glUniform3fv(light, 1, glm::value_ptr(lightPosition));

	// TODO: Set eye position (camera position) uniform
	glm::vec3 eyePosition = GetSceneCamera()->transform->GetWorldPosition();
	int eye = glGetUniformLocation(shader->program, "eye_position");
	glUniform3fv(eye, 1, glm::value_ptr(eyePosition));

	// TODO: Set material property uniforms (shininess, kd, ks, object color) 
	int prop = glGetUniformLocation(shader->program, "material_shininess");
	glUniform1i(prop, materialShininess);

	prop = glGetUniformLocation(shader->program, "material_kd");
	glUniform1f(prop, materialKd);

	prop = glGetUniformLocation(shader->program, "material_ks");
	glUniform1f(prop, materialKs);

	prop = glGetUniformLocation(shader->program, "object_color");
	glUniform3fv(prop, 1, glm::value_ptr(color));

	float ang_ = glGetUniformLocation(shader->program, "ang");
	glUniform1f(ang_, Ang);

	int sp = glGetUniformLocation(shader->program, "sp");
	glUniform1i(sp, Sp);

	// Bind model matrix
	GLint loc_model_matrix = glGetUniformLocation(shader->program, "Model");
	glUniformMatrix4fv(loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));
	
	// Bind view matrix
	glm::mat4 viewMatrix = GetSceneCamera()->GetViewMatrix();
	int loc_view_matrix = glGetUniformLocation(shader->program, "View");
	glUniformMatrix4fv(loc_view_matrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));

	// Bind projection matrix
	glm::mat4 projectionMatrix = GetSceneCamera()->GetProjectionMatrix();
	int loc_projection_matrix = glGetUniformLocation(shader->program, "Projection");
	glUniformMatrix4fv(loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	// Draw the object
	glBindVertexArray(mesh->GetBuffers()->VAO);
	glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}

// Documentation for the input functions can be found in: "/Source/Core/Window/InputController.h" or
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/Window/InputController.h

void Laborator7::OnInputUpdate(float deltaTime, int mods)
{
	if (window->KeyHold(GLFW_KEY_SPACE))
		space = 1;
	// move the camera only if MOUSE_RIGHT button is pressed
	if (window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		float cameraSpeed = 2.0f;

		if (window->KeyHold(GLFW_KEY_L)) {
			projectionMatrix = glm::ortho(-2.0f, 2.0f, -2.0f, 2.0f, 0.01f, 200.0f);
		}

		if (window->KeyHold(GLFW_KEY_K)) {
			projectionMatrix = glm::ortho(-4.0f, 4.0f, -4.0f, 4.0f, 0.01f, 200.0f);
		}

		if (window->KeyHold(GLFW_KEY_M)) {
			projectionMatrix = glm::perspective(RADIANS(40), window->props.aspectRatio, 0.01f, 200.0f);
		}

		if (window->KeyHold(GLFW_KEY_N)) {
			projectionMatrix = glm::perspective(RADIANS(110), window->props.aspectRatio, 0.01f, 200.0f);
		}

		if (window->KeyHold(GLFW_KEY_O)) {
			projectionMatrix = glm::ortho(-3.0f, 3.0f, -3.0f, 3.0f, 0.01f, 200.0f);

		}

		if (window->KeyHold(GLFW_KEY_P)) {
			projectionMatrix = glm::perspective(RADIANS(60), window->props.aspectRatio, 0.01f, 200.0f);

		}

		if (window->KeyHold(GLFW_KEY_W)) {
			// TODO : translate the camera forward
			camera->TranslateForward(deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_A)) {
			// TODO : translate the camera to the left
			camera->TranslateRight(-deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_S)) {
			// TODO : translate the camera backwards
			camera->TranslateForward(-deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_D)) {
			// TODO : translate the camera to the right
			camera->TranslateRight(deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_Q)) {
			// TODO : translate the camera down
			camera->TranslateUpword(-deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_E)) {
			// TODO : translate the camera up
			camera->TranslateUpword(deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_C)) {
			// TODO : translate the camera backwards
			camera->TranslateForward(deltaTime * 100);
		}
	}
}

void Laborator7::OnKeyPress(int key, int mods)
{
	// add key press event
	
}

void Laborator7::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator7::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
	if (window->MouseHold(GLFW_MOUSE_BUTTON_LEFT))
	{
		mouse = 1;
		deltay = float(deltaY);
	}
	if (window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		float sensivityOX = 0.001f;
		float sensivityOY = 0.001f;

		if (window->GetSpecialKeyState() == 0) {
			renderCameraTarget = false;
			// TODO : rotate the camera in First-person mode around OX and OY using deltaX and deltaY
			// use the sensitivity variables for setting up the rotation speed
			camera->TranslateRight(sensivityOX * deltaX);
			//camera->RotateFirstPerson_OY(sensivityOY * deltaY);
		}

		if (window->GetSpecialKeyState() && GLFW_MOD_CONTROL) {
			renderCameraTarget = true;
			// TODO : rotate the camera in Third-person mode around OX and OY using deltaX and deltaY
			// use the sensitivity variables for setting up the rotation speed
			camera->RotateThirdPerson_OX(sensivityOX * deltaX);
			camera->RotateThirdPerson_OY(sensivityOY * deltaY);
		}
	}
}

void Laborator7::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator7::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator7::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator7::OnWindowResize(int width, int height)
{
}
