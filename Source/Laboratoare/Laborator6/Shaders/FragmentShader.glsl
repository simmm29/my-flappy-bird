#version 330

// TODO: get values from fragment shader
in vec2 fragment_coord;
in vec3 fragment_normal;
in vec3 fragment_color;
in vec3 fragment_position;

layout(location = 0) out vec4 out_color;

void main()
{
	// TODO: write pixel out color
	//out_color = vec4(fragment_color, 1);
	out_color = vec4(fragment_normal, 1);
}