#pragma once
#include <Component/SimpleScene.h>
#include <Component/Transform/Transform.h>
#include <Core/GPU/Mesh.h>
#include "Transform3D.h"
#include "Camera.h"
class Tema2 : public SimpleScene
{
public:
	Tema2();
	~Tema2();

	void Init() override;

private:
	void FrameStart() override;
	void Update(float deltaTimeSeconds) override;
	void FrameEnd() override;

	void RenderSimpleMesh(Mesh* mesh, Shader* shader, const glm::mat4& modelMatrix, const glm::vec3& color = glm::vec3(1));

	void OnInputUpdate(float deltaTime, int mods) override;
	void OnKeyPress(int key, int mods) override;
	void OnKeyRelease(int key, int mods) override;
	void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
	void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
	void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
	void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
	void OnWindowResize(int width, int height) override;

protected:
	Labor::Camera* camera;
	glm::vec3 lightPosition;
	bool renderCameraTarget;
	float counter = 0, doi = 20.0;
	int n = 0, kol = 0;
	float d = 20.0;
	int col_bonus1 = 0, col_bonus2;
	float o_1 = 3, o_2 = 1, o_3 = 5, c_1, c_2, c_3, s_1 = -5.5, e_1 = 3, s_2 = -7.5, e_2 = 1, s_3 = -3.5, e_3 = 5;
	float o_5 = 10, s_5 = -3.8, e_5 = 10, c_5;
	float o_4 = 7, s_4 = -3.2, e_4 = 7, c_4;
	float b_1, a_1 = 2, start_1 = -6.2, end_1 = 2, b_2, a_2 = 6, start_2 = -3.2, end_2 = 6, b_3, a_3 = 4, start_3 = -4.2, end_3 = 4;
	float angularStep, angie_1 = 0.5, angie_2 = 1, angie = 0, tx, ty, tz, deltax, deltay, ang, y_1, cici, translateX, translateY, translateZ;
	int mouse, space, x = 0.35, y = 2.5, k, ok = 0, coliziune, coliziune_2, coliziune_3;
	unsigned int materialShininess;
	float materialKd;
	float materialKs;
	int col = 0;
	float bust = 1.7;
	float ana, simina;
	int ok1 = 0, ok2 = 0, ok3 = 0, ok4 = 0;
	int h = 0, flag = 0;
	float u, u1, u2, u3, u4, u5, u6;
	int sp, f, Sp = 0;
	float ang_, Ang;
	int q = 0;
	glm::mat4 projectionMatrix;
};
