#pragma once

#include <Component/SimpleScene.h>
#include <string>
#include <Core/Engine.h>

class Tema1 : public SimpleScene
{
public:
	Tema1();
	~Tema1();

	void Init() override;

private:
	void FrameStart() override;
	void Update(float deltaTimeSeconds) override;
	void FrameEnd() override;

	void OnKeyPress(int key, int mods) override;
	void OnKeyRelease(int key, int mods) override;
	
protected:
	glm::mat3 modelMatrix;
	float x1, x2, x3, x4, y1_1, y1_2, y2_1, y2_2, y3_1, y3_2, y4_1, y4_2;
	float ty = 0;
	float scaleX, scaleY, scaleX_1, scaleY_1;
	float angularStep;
	int length_1, width_1, length_2, width_2, length_3, width_3;
	float radius, rad;
	float side, side_1, side_2;
	int coliziune = 0;
	int ok_1 = 0, ok_2 = 0, ok_3 = 0, ok_4 = 0;
	int sare = 0, cadere = 0, cadere_1 = 0, stop = 0, scor = 0;
};
