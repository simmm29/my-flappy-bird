#include "Tema1.h"

#include <vector>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <Core/Engine.h>
#include "Transform2D.h"
#include "Object2D.h"

using namespace std;
Tema1::Tema1()
{
}

Tema1::~Tema1()
{
}

void Tema1::Init()
{
	glm::ivec2 resolution = window->GetResolution();
	auto camera = GetSceneCamera();
	camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	GetCameraInput()->SetActive(false);

	glm::vec3 corner = glm::vec3(0, 0, 0);
	glm::vec3 center = glm::vec3(100, 100, 100);

	x1 = 1200;
	x2 = 1500;
	x3 = 1700;
	x4 = 2100;

	scaleX = 1;
	scaleY = 1;
	scaleX_1 = 1;
	scaleY_1 = 1;

	angularStep = 0;
	radius = 30;
	rad = 3;
	side = 30;
	side_1 = 20;
	side_2 = 18;

	length_1 = 40;
	width_1 = 60;

	length_2 = 80;
	width_2 = 400;

	length_3 = 45;
	width_3 = 45;

	Mesh* rectangle = Object2D::CreateRectangle("rectangle", corner, width_1, length_1, glm::vec3(0.941, 0.902, 0.549), true);
	AddMeshToList(rectangle);

	Mesh* rectangle_1 = Object2D::CreateRectangle("rectangle_1", corner, width_2, length_2, glm::vec3(0.1f, 0.0f, 0.1f), true);
	AddMeshToList(rectangle_1);

	Mesh* triangle = Object2D::CreateTriangle("triangle", corner, side, glm::vec3(0.980, 0.502, 0.447), true);
	AddMeshToList(triangle);

	Mesh* triangle_1 = Object2D::CreateTriangle("triangle_1", corner, side_1, glm::vec3(0.941, 0.902, 0.549), true);
	AddMeshToList(triangle_1);

	Mesh* triangle_2 = Object2D::CreateTriangle("triangle_2", corner, side, glm::vec3(0.941, 0.902, 0.549), true);
	AddMeshToList(triangle_2);

	Mesh* triangle_3 = Object2D::CreateTriangle("triangle_3", corner, side_2, glm::vec3(0.957, 0.643, 0.376), true);
	AddMeshToList(triangle_3);

	Mesh* rectangle_3 = Object2D::CreateRectangle("rectangle_3", corner, width_3, length_3, glm::vec3(0.941, 0.902, 0.549), true);
	AddMeshToList(rectangle_3);

	Mesh* circle = Object2D::CreateCircle("circle", corner, radius, glm::vec3(0.941, 0.902, 0.549), true);
	AddMeshToList(circle);

	Mesh* circle_1 = Object2D::CreateCircle("circle_1", corner, rad, glm::vec3(0.824, 0.412, 0.118), true);
	AddMeshToList(circle_1);

}

void Tema1::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(1, 0, 1, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}
float clamp(float value, float min, float max) {
	return std::max(min, std::min(max, value));
}
void Tema1::Update(float deltaTimeSeconds)
{
	// daca apas SPACE si daca pasarea nu s-a lovit de niciun pipe, ty incepe sa creasca(pasarea se va translata in sus pe OY)
	if (coliziune == 0 && sare == 1) {
		if (cadere_1 == 0) {
			// scalarea aripii pasarii pentru a simula zborul atunci cand pasarea e in urcare
			scaleY_1 += deltaTimeSeconds * 2;
			scaleX_1 += deltaTimeSeconds * 2;
		}
		if (scaleY_1 > 1.3 || scaleX_1 > 1.3) {
			scaleY_1 = 0.5;
			scaleX_1 = 0.5;
		}
		ty += deltaTimeSeconds * 400;
		// rotirea pasarii nu va depasi acest prag
		if (angularStep <= 6)
			angularStep += deltaTimeSeconds * 10;
	}
	else {
		// altfel, daca totusi nu a avut loc coliziune, ty e in continua scadere (pasarea tinde sa cada)
		if (coliziune == 0) {
			scaleY_1 = 1;
			scaleX_1 = 1;
			if (angularStep > 5)
				angularStep -= deltaTimeSeconds;
			else
				angularStep = 5;
			ty -= deltaTimeSeconds * 200;
		}
		else {
			// daca a avut loc o coliziune, pasarea va cobori extrem de repede
			ty -= deltaTimeSeconds * 600;
		}
	}

	// ochi pasare
	// il translatez continuu pe OY, aplicandu-i o rotatie(angularStep se modifica in functie de apasarea tastei SPACE- creste pana la un prag
	// odata cu apasarea ei, descreste pana la alt prag altfel
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(0, ty);
	modelMatrix *= Transform2D::Translate(650, 250);
	modelMatrix *= Transform2D::Translate(50, 50);
	modelMatrix *= Transform2D::Rotate(angularStep);
	modelMatrix *= Transform2D::Translate(-50, -50);
	modelMatrix *= Transform2D::Translate(-5, 13);
	RenderMesh2D(meshes["circle_1"], shaders["VertexColor"], modelMatrix);

	//aripa pasare

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(0, ty);
	modelMatrix *= Transform2D::Translate(650, 270);
	modelMatrix *= Transform2D::Translate(50, 50);
	modelMatrix *= Transform2D::Rotate(angularStep);
	modelMatrix *= Transform2D::Translate(-50, -50);
	modelMatrix *= Transform2D::Translate(-50, -35);
	modelMatrix *= Transform2D::Scale(0.7, 0.7);
	modelMatrix *= Transform2D::Scale(scaleX_1, scaleY_1); // pentru miscarea aripii pasarii
	RenderMesh2D(meshes["triangle_2"], shaders["VertexColor"], modelMatrix);

	//cioc pasare

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(0, ty);
	modelMatrix *= Transform2D::Translate(650, 250);
	modelMatrix *= Transform2D::Translate(50, 50);
	modelMatrix *= Transform2D::Rotate(angularStep);
	modelMatrix *= Transform2D::Translate(-50, -50);
	RenderMesh2D(meshes["triangle_3"], shaders["VertexColor"], modelMatrix);

	//cap pasare

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(0, ty);
	modelMatrix *= Transform2D::Translate(650, 250);
	modelMatrix *= Transform2D::Translate(50, 50);
	modelMatrix *= Transform2D::Rotate(angularStep);
	modelMatrix *= Transform2D::Translate(-50, -50);
	modelMatrix *= Transform2D::Translate(-2, -2);
	modelMatrix *= Transform2D::Scale(0.7, 0.7);
	RenderMesh2D(meshes["circle"], shaders["VertexColor"], modelMatrix);

	//corp pasare

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(0, ty);
	modelMatrix *= Transform2D::Translate(650, 250);
	modelMatrix *= Transform2D::Translate(50, 50);
	modelMatrix *= Transform2D::Rotate(angularStep);
	modelMatrix *= Transform2D::Translate(-50, -50);
	modelMatrix *= Transform2D::Translate(-30, -30);
	RenderMesh2D(meshes["circle"], shaders["VertexColor"], modelMatrix);

	// mediu

	// prima pereche de dreptunghiuri (se comporta ca niste usi glisante)
	// generez de fiecare data noi inaltimi pentru perechea mea de dreptunghiuri prin modificarea y fiecarui patrat la translatie
	if (y1_1 == 0) { 
		y1_1 = (rand() % (540 - 670)) + 540;
		y1_2 = (rand() % (0 + 200)) - 200;
		x1 -= deltaTimeSeconds * 200; // perechea de dreptunghiuri se translateaza continuu de la dreapta la stanga pe OX
	}
	else {
		// daca am ajuns la marginea stanga a ferestrei, o iau de la capat, pornind cu un x destul de mare, pentru a crea continuitate
		if (x1 < -80) {
			x1 = 1300;
			y1_1 = (rand() % (540 - 670)) + 540;
			y1_2 = (rand() % (0 + 200)) - 200;
		}
		else
			x1 -= deltaTimeSeconds * 200;
	}
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(x1, y1_1 + 200);
	// usile glisante sunt realizate printr-o scalare continua pe ambele axe, intre 2 praguri
	if (cadere == 0)
		scaleY -= deltaTimeSeconds * 0.5;
	if (scaleY < -0.8) {
		cadere = 1;
	}
	if (cadere == 1)
		scaleY += deltaTimeSeconds * 0.5;
	if (scaleY > 0)
		cadere = 0;
	modelMatrix *= Transform2D::Scale(scaleX, scaleY);
	RenderMesh2D(meshes["rectangle_1"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(x1, y1_2);
	modelMatrix *= Transform2D::Scale(scaleX, -scaleY);
	RenderMesh2D(meshes["rectangle_1"], shaders["VertexColor"], modelMatrix);

	//a doua

	if (y2_1 == 0) {
		y2_1 = (rand() % (540 - 670)) + 540;
		y2_2 = (rand() % (0 + 200)) - 200;
		x2 -= deltaTimeSeconds * 200;
	}
	else {
		if (x2 < -80) {
			x2 = 1300;
			y2_1 = (rand() % (540 - 670)) + 540;
			y2_2 = (rand() % (0 + 200)) - 200;
		}
		else
			x2 -= deltaTimeSeconds * 200;
	}
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(x2, y2_1);
	RenderMesh2D(meshes["rectangle_1"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(x2, y2_2);
	RenderMesh2D(meshes["rectangle_1"], shaders["VertexColor"], modelMatrix);


	//a treia

	if (y3_1 == 0) {
		y3_1 = (rand() % (540 - 670)) + 540;
		y3_2 = (rand() % (0 + 200)) - 200;
		x3 -= deltaTimeSeconds * 200;
	}
	else {
		if (x3 < -80) {
			x3 = 1300;
			y3_1 = (rand() % (540 - 670)) + 540;
			y3_2 = (rand() % (0 + 200)) - 200;
		}
		else
			x3 -= deltaTimeSeconds * 200;
	}
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(x3, y3_1);
	RenderMesh2D(meshes["rectangle_1"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(x3, y3_2);
	RenderMesh2D(meshes["rectangle_1"], shaders["VertexColor"], modelMatrix);

	//a patra

	if (y4_1 == 0) {
		y4_1 = (rand() % (540 - 670)) + 540;
		y4_2 = (rand() % (0 + 200)) - 200;
		x4 -= deltaTimeSeconds * 200;
	}
	else {
		if (x4 < -80) {
			x4 = 1300;
			y4_1 = (rand() % (540 - 670)) + 540;
			y4_2 = (rand() % (0 + 200)) - 200;
		}
		else
			x4 -= deltaTimeSeconds * 200;
	}
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(x4, y4_1);
	RenderMesh2D(meshes["rectangle_1"], shaders["VertexColor"], modelMatrix);
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(x4, y4_2);
	RenderMesh2D(meshes["rectangle_1"], shaders["VertexColor"], modelMatrix);

	//soare

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(100, 600);
	modelMatrix *= Transform2D::Scale(3, 3);
	RenderMesh2D(meshes["circle"], shaders["VertexColor"], modelMatrix);


	//munti

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(50, -500);
	modelMatrix *= Transform2D::Scale(15, 20);
	modelMatrix *= Transform2D::Rotate(7.07);
	RenderMesh2D(meshes["triangle"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(300, -200);
	modelMatrix *= Transform2D::Scale(5, 10);
	modelMatrix *= Transform2D::Rotate(7.07);
	RenderMesh2D(meshes["triangle"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(600, -430);
	modelMatrix *= Transform2D::Scale(10, 20);
	modelMatrix *= Transform2D::Rotate(7.07);
	RenderMesh2D(meshes["triangle"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(800, -430);
	modelMatrix *= Transform2D::Scale(7, 15);
	modelMatrix *= Transform2D::Rotate(7.07);
	RenderMesh2D(meshes["triangle"], shaders["VertexColor"], modelMatrix);

	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(1100, -550);
	modelMatrix *= Transform2D::Scale(15, 25);
	modelMatrix *= Transform2D::Rotate(7.07);
	RenderMesh2D(meshes["triangle"], shaders["VertexColor"], modelMatrix);

	// coliziuni

	//coliziuni cu dreptunghiurile de sus
	bool coliziuneX_1_sus = (650 + side) > x1 && (x1 + width_2) > 650;
	bool coliziuneY_1_sus = (ty + side + 200) > y1_1 && (y1_1 + length_2) > (200 + ty);

	// pentru axa OX, daca partea dreapta a pasarii e mai mare decat partea stanga a pipe-ului curent si daca partea dreapta a pipe-ului e mai mare decat partea stanga a pasarii
	bool coliziuneX_2_sus = (650 + side) > x2 && (x2 + width_2) > 650;
	// similar si pentru axa OY
	bool coliziuneY_2_sus = (ty + side + 200) > y2_1 && (y2_1 + length_2) > (200 + ty);

	bool coliziuneX_3_sus = (650 + side) > x3 && (x3 + width_2) > 650;
	bool coliziuneY_3_sus = (ty + side + 200) > y3_1 && (y3_1 + length_2) > (200 + ty);

	bool coliziuneX_4_sus = (650 + side) > x4 && (x4 + width_2) > 650;
	bool coliziuneY_4_sus = (ty + side + 200) > y4_1 && (y4_1 + length_2) > (200 + ty);

	//coliziuni cu dreptunghiurile de jos
	bool coliziuneX_1_jos = (650 + side) > x1 && (x1 + width_2) > 650;
	bool coliziuneY_1_jos = (ty + side) > y1_2 && (y1_2 + length_2) > ty;

	bool coliziuneX_2_jos = (650 + side) > x2 && (x2 + width_2) > 650;
	bool coliziuneY_2_jos = (ty + side) > y2_2 && (y2_2 + length_2) > ty;

	bool coliziuneX_3_jos = (650 + side) > x3 && (x3 + width_2) > 650;
	bool coliziuneY_3_jos = (ty + side) > y3_2 && (y3_2 + length_2) > ty;

	bool coliziuneX_4_jos = (650 + side) > x4 && (x4 + width_2) > 650;
	bool coliziuneY_4_jos = (ty + side) > y4_2 && (y4_2 + length_2) > ty;

	// daca pe ambele axe exista suprapuneri, inseamna ca a avut loc o coliziune
	if (coliziuneX_1_jos == true && coliziuneY_1_jos == true) {
		coliziune = 1;
	}
	if (coliziuneX_2_jos == true && coliziuneY_2_jos == true) {
		coliziune = 1;
	}
	if (coliziuneX_3_jos == true && coliziuneY_3_jos == true) {
		coliziune = 1;
	}
	if (coliziuneX_4_jos == true && coliziuneY_4_jos == true) {
		coliziune = 1;
	}
	if (coliziuneX_1_sus == true && coliziuneY_1_sus == true) {
		coliziune = 1;
	}
	if (coliziuneX_2_sus == true && coliziuneY_2_sus == true) {
		coliziune = 1;
	}
	if (coliziuneX_3_sus == true && coliziuneY_3_sus == true) {
		coliziune = 1;
	}
	if (coliziuneX_4_sus == true && coliziuneY_4_sus == true) {
		coliziune = 1;
	}

	// scorul curent si final, afisate in consola
	
	// daca pasarea e inca in joc (se afla in fereastra, nu mai jos)
	if (ty > -400) {
		if (650 > x1 && ok_1 == 0 && x1 > 0) {
			scor++;
			ok_1 = 1; // semafor ca am trecut de prima pereche
			cout << scor << "\n";
		}
		if (650 > x2 && ok_2 == 0 && x2 > 0) {
			scor++;
			ok_2 = 1;
			cout << scor << "\n";
		}
		if (650 > x3 && ok_3 == 0 && x3 > 0) {
			scor++;
			ok_3 = 1;
			cout << scor << "\n";
		}
		if (650 > x4 && ok_4 == 0 && x4 > 0) {
			scor++;
			ok_4 = 1;
			cout << scor << "\n";
		}

		// daca perechea curenta nu se mai afla in fereastra, toate semafoarele redevin 0, pentru a putea modifica
		// o data pe fereastra scorul (trecerea de o pereche de dreptunghiuri sa fie adaugata o singura data)
		if (x1 < 0)
			ok_1 = 0;
		if (x2 < 0)
			ok_2 = 0;
		if (x3 < 0)
			ok_3 = 0;
		if (x4 < 0)
			ok_4 = 0;
	}

	// daca pasarea s-a atins de vreun pipe sau nu se mai afla in spatiul de joc, va cadea mult mai repede, reprezentand finalul jocului
	if (coliziune == 1 || ty <= -400) {
		ty -= deltaTimeSeconds * 600;
	}

	//verificare ca pasarea a pierdut
	if (ty <= -1000) {
		cout << "scor final e " << scor - 1<< "\n";
		exit(1);
	}

}


void Tema1::FrameEnd()
{

}



void Tema1::OnKeyPress(int key, int mods)
{
	// add key press event
	if (key == GLFW_KEY_SPACE)
		sare = 1;

}

void Tema1::OnKeyRelease(int key, int mods)
{
	// add key release event
	if (key == GLFW_KEY_SPACE)
		sare = 0;
}
